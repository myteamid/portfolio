import React from 'react'
/*custom components*/
import { Socials } from '../../components/socials'
import { Sandwich } from '../../components/sandwich'
/*styles*/
import './style.scss'

export let Header = () => {
  return (
    <header className="header">
      <div className="container-fluid">
        <div className="header_inner">
          <div className="header_social">
            <Socials/>
          </div>
          <Sandwich/>
        </div>
      </div>
    </header>
  )
}