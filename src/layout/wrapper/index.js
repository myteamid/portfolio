import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
/*pages*/
import { Home } from '../../pages/Home'
import { Blog } from '../../pages/Blog'
import { Portfolio } from '../../pages/Portfolio'
import LoginPage from '../../pages/Login'
import { Nav } from '../../components/nav'
import Container from '../../pages'
import { Footer } from '../footer/footer'

const Wrapper = () => {

  return (
    <div className="App">
      <Container>
        <TransitionGroup className="csstra">
          <CSSTransition key={12} classNames="fade" timeout={300}>
            <div>
              <Nav/>
              <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/blog' component={Blog}/>
                <Route path='/portfolio' component={Portfolio}/>
                <Route path='/login' component={LoginPage}/>
              </Switch>
              <Footer/>
            </div>
          </CSSTransition>
        </TransitionGroup>
      </Container>
    </div>
  )
}

export default Wrapper

