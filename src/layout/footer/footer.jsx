import React from 'react'
import { Link } from 'react-router-dom'
/*custom*/
import { Socials } from '../../components/socials'
import Routing from '../../api/Routing'
import './style.scss'

export let Footer = () => {
  const renderNav = () => (
    Routing.map(item => (
      <li className="footer_nav-item" key={item.id}>
        <Link to={item.path}>
          {item.title}
        </Link>
      </li>
    ))
  )

  return (
    <footer className="footer">
      <div className="container-fluid">
        <div className="footer_inner">
          <div className="footer_top">
            <ul className="footer_nav">
              {renderNav()}
            </ul>
            <div className="footer_socials">
              <Socials className={'socials--border'}/>
            </div>
          </div>
        </div>
      </div>
      <hr/>
      <div className="container-fluid">
        <div className="footer_inner">
          <div className="footer_bottom">
            <p>
              © Роман Таран | 2018 </p>
          </div>
        </div>
      </div>
    </footer>
  )
}