import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools }          from 'redux-devtools-extension'
import loadApi               from '../middlewares/loadApi'
import reduser              from '../reduser'
import createBrowserHistory from "history/createBrowserHistory"
import { routerMiddleware } from "react-router-redux"
import thunk from 'redux-thunk'


export const history = createBrowserHistory();
const middleware = routerMiddleware(history);

const composeEnhancers = composeWithDevTools({
    // Specify here name, actionsBlacklist, actionsCreators and other options

});

export const store = createStore(reduser, composeEnhancers(
    applyMiddleware(thunk, middleware, loadApi)
))