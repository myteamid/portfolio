import {
    IS_LOAD_THEME_ERROR,
    IS_LOAD_THEME_SUCCESS,
    IS_LOAD_THEME_LOADING,
    IS_LOAD_THEME_ITEM_SUCCESS,
    TOGGLE_THEME
}                    from '../constants'
import { themeData } from '.././api/themesData'


export function activeTheme(state = themeData[ 0 ], action) {
    const { type, targetTheme } = action;
    switch (type) {
        case TOGGLE_THEME:
            // console.log(action);
            return targetTheme;
        default:
            return state
    }
}


export function isLoadThemeError(state = false, action) {
    const { type, hasError } = action;
    switch (type) {
        case IS_LOAD_THEME_ERROR:
            return hasError;
        default:
            return state;
    }
}


export function isLoadThemeLoading(state = false, action) {
    const { type, isLoading } = action;
    switch (type) {
        case IS_LOAD_THEME_LOADING:
            return isLoading;
        default:
            return state;
    }
}

export function loadTheme(state = [], action) {
    const { type, dataThemeColor } = action;
    switch (type) {
        case IS_LOAD_THEME_SUCCESS:
            return dataThemeColor;
        default:
            return state;
    }
}

export function loadThemeItem(state = [], action) {
    const { type, themeItemData } = action;
    // console.log(action);
    switch (type) {
        case IS_LOAD_THEME_ITEM_SUCCESS:
            return themeItemData;
        default:
            return state;
    }
}

