import { combineReducers } from 'redux'
import { routerReducer }   from 'react-router-redux'

import openMenuReducer from './openMenu'
import {activeTheme}                 from './colorTheme'


export default combineReducers({
    openMenu: openMenuReducer,
    activeTheme,
    routing: routerReducer
})