export const skills = [
	{
		'id': '1',
		'name': 'Front-end',
		'skillItem': [
			{
				'id': 'qweqwe',
				'name': 'HTML5',
				'level': 90
			},
			{
				'id': 'asdaf',
				'name': 'CSS3',
				'level': 85
			},
			{
				'id': 'fwfwf',
				'name': 'JS & JQuery',
				'level': 80
			},
			{
				'id': '23rwfs',
				'name': 'React JS',
				'level': 65
			}
		]
	},
	{
		'id': '2',
		'name': 'Back-end',
		'skillItem': [
			{
				'id': 'gngnfb',
				'name': 'PHP',
				'level': 45
			},
			{
				'id': 'mfbfbfb',
				'name': 'mySQL',
				'level': 35
			},
			{
				'id': 'sdfsad',
				'name': 'Wordpress',
				'level': 50
			}
		]
	},
	{
		'id': '3',
		'name': 'Workflow',
		'skillItem': [
			{
				'id': 'yg4rhhd',
				'name': 'npm',
				'level': 60
			},
			{
				'id': 'ascbbb',
				'name': 'Git',
				'level': 80
			},
			{
				'id': 'werg4',
				'name': 'Gulp',
				'level': 70
			},
			{
				'id': '4gdbxv',
				'name': 'Webpack',
				'level': 55
			}
		]
	}
];

