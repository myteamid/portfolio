export const themeData = [
    {
        "id": 1,
        "themeId": 1,
        "title": "Night",
        "color": "blue-black",
        "styles": [
            {
                "--main-color-1": "#373e42",
                "--main-color-2": "#566358",
                "--main-color-3": "#4e8839",
                "--main-color-4": "#6c9c5a",
                "--main-color-4-a": "#6c9c5a",
                "--main-color-5": "#ebefe2",
                "--main-color-6": "#f4f5f0"
            }
        ],
        "images": [
            {"bgHero": 'https://image.ibb.co/bwZPdz/night.jpg'},
            {"bgForm": '../../assets/images/night/form.png'},
        ]
    },
    {
        "id": 2,
        "themeId": 2,
        "title": "Forest",
        "color": "blue",
        "styles": [
            {
                "--main-color-1": "#fff",
                "--main-color-2": "#fcc",
                "--main-color-3": "#fec",
                "--main-color-4": "#6fc",
                "--main-color-4-a": "#fd2",
                "--main-color-5": "#ee2",
                "--main-color-6": "#f4f5f0"
            }
        ],
        "images": [
            {"bgHero": 'https://image.ibb.co/bwZPdz/night.jpg'},
            {"bgForm": '../../assets/images/mountains/form.png'},
        ]
    },
    {
        "id": 3,
        "themeId": 3,
        "title": "Mountains",
        "color": "green",
        "styles": [
            {
                "--main-color-1": "#373e42",
                "--main-color-2": "#566358",
                "--main-color-3": "#4e8839",
                "--main-color-4": "#6c9c5a",
                "--main-color-4-a": "#6c9c5a",
                "--main-color-5": "#ebefe2",
                "--main-color-6": "#f4f5f0"
            }
        ],
        "images": [
            {"bgHero": 'https://image.ibb.co/bwZPdz/night.jpg'},
            {"bgForm": '../../assets/images/mountains/form.png'},
        ]
    },
    {
        "id": 4,
        "themeId": 4,
        "title": "Desert",
        "color": "orange",
        "styles": [
            {
                "--main-color-1": "#373e42",
                "--main-color-2": "#566358",
                "--main-color-3": "#4e8839",
                "--main-color-4": "#6c9c5a",
                "--main-color-4-a": "#6c9c5a",
                "--main-color-5": "#ebefe2",
                "--main-color-6": "#f4f5f0"
            }
        ],
        "images": [
            {"bgHero": 'https://lh3.google.com/u/0/d/1qkG4WRnpvEGR6HaynqjVuY72mKpdZThw=w1920-h610-iv1'},
            {"bgForm"   : '../../assets/images/mountains/form.png'},
        ]
    }
];