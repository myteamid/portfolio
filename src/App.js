import React, { Component } from 'react';

import { Provider } from 'react-redux'
import { ConnectedRouter }   from 'react-router-redux'


import Wrapper            from './layout/wrapper'
import { history, store } from "./store";


class App extends Component {

	render() {
		return (
			<Provider store={store}>
				<ConnectedRouter history={history}>

					<Wrapper />

				</ConnectedRouter>
			</Provider>
		);
	}
};


export default App;


