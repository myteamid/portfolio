import RouteContainer  from './RouteContainer';
import { connect }     from 'react-redux';
import { withRouter }  from 'react-router'

/*custom*/
import { closeMenu }  from '../AC'

function mapStateToProps(state) {
	return {
		open: state.openMenu,
        activeTheme: state.activeTheme
	};
}

const mapDispatchToProps = { closeMenu };

const decorator = connect(mapStateToProps, mapDispatchToProps);

export default withRouter(decorator(RouteContainer));