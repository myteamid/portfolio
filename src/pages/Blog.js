import React from 'react'
/*custom component*/
import { Hero } from '../components/hero'
import { Header } from '../layout/header/header'
import { BlogContainer } from '../components/blogContainer'

export let Blog = () => {

  return (
    <main>
      <Header/>
      <Hero>
        Личный сайт веб разработчика
      </Hero>
      <BlogContainer/>
    </main>
  )
}