import React, { Component } from 'react'

export default class RouteContainer extends Component {
  constructor (props) {
    super(props)

    this.props.history.listen((location, action) => {
      this.props.closeMenu()
    })
  }

  render () {
    const {children, activeTheme} = this.props
    return (
      <div className={`theme-${activeTheme.color}`}>
        {children}
      </div>
    )
  }
}