import React from 'react'
/*custom*/
import { Login } from '../components/login'

let LoginPage = () => {
    return (
      <main className='main'>
        <Login />
      </main>
    )
}
export default LoginPage;