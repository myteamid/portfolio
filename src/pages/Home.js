import React from 'react'
/*data*/
import { skills } from '../api/Skills'
/*custom component*/
import { Hero } from '../components/hero'
import { About } from '../components/about'
import { Gmap } from '../components/gMap/gMap'
import { Header } from '../layout/header/header'

export let Home = () => {

  return (
    <main>
      <Header/>
      <Hero>
        Личный сайт веб разработчика
      </Hero>
      <About skillData={skills}/>
      <Gmap/>
    </main>
  )
}