import React from 'react'
/*custom component*/
import { Hero } from '../components/hero'
import { PortfolioSection } from '../components/portfolioSection'
import { ContactForm } from '../components/contactForm'
import { Header } from '../layout/header/header'

export let Portfolio = () => {

  return (
    <main>
      <Header/>
      <Hero>Личный сайт веб разработчика</Hero>
      <PortfolioSection/>
      <ContactForm/>
    </main>
  )
}