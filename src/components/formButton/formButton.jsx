import React from 'react'

export let FormButton = (props)=> {
  const {children, ...rest} = props
  return (
    <button {...rest}>
      {children}
    </button>
  )
}