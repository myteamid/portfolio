import React, { Component } from 'react'
import CSSTransitionGroup from 'react-addons-css-transition-group'
/*styles*/
import './style.scss'
/*custom*/
import { Article } from '../article'
import { ArticleForm as PopUp } from '../articleForm'

export class BlogContainer extends Component {

  state = {
    formOpen: false,
  }

  handleForm = () => {
    this.setState({
      formOpen: !this.state.formOpen,
    })
  }

  renderPopUp = () => {
    return (
      <PopUp>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos,
          nobis!</p>
      </PopUp>
    )
  }

  render () {
    const transitionClassName = {
      enter: 'fade_animation-enter',
      enterActive: 'fade_animation-enterActive',
      leave: 'fade_animation-leave',
      leaveActive: 'fade_animation-leaveActive',
    }
    return (
      <div className="blog">
        <div className="blog_left-line"></div>
        <div className="blog_right-line"></div>

        <div className="blog_right">
          <div className="blog_form">
            <button className="blog_btn btn" onClick={this.handleForm}>Add
              article
            </button>
            <CSSTransitionGroup transitionName={transitionClassName}
                                transitionEnterTimeout={1200}
                                transitionLeaveTimeout={600}>
              {this.state.formOpen ?
                <form className="blog_form-inner">
                  <div className="form_block">
                    <label>
                      <input type="text" className="input"
                             placeholder="Your Name"/>
                    </label>
                  </div>
                  <div className="form_block">
                    <label>
                      <input type="email" className="input"
                             placeholder="Your Email"/>
                    </label>
                  </div>
                  <div className="form_block">
                    <label>
                      <textarea name="articleContent" id="" cols="30" rows="10"
                                placeholder="Your article" className="input">
                      </textarea>
                    </label>
                  </div>
                  <button type="submit" className="btn-main">Add article
                  </button>
                </form>
                : ''}
            </CSSTransitionGroup>
          </div>
          <Article/>
          <Article/>
        </div>

        {this.renderPopUp()}
      </div>
    )
  }
}

BlogContainer.propTypes = {}
