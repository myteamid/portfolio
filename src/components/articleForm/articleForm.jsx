import React from 'react'

export let ArticleForm = (props) => {

  return (
    <div className="article-form">
      <div className="article-form_inner">
        {props.children}
      </div>
    </div>
  )
}