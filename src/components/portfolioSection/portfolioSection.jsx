import React from 'react'
/*custom*/
import { PortfolioSlider } from '../portfolioSlider'
/*img*/
import SvgWorks from '../../assets/images/icon/works_header.svg'
/*styles*/
import './style.scss'

export let PortfolioSection = () => {
  return (
    <section className="portfolio">
      <div className="container-fluid">
        <div className="portfolio_title">
          <div className="portfolio_title-bg is-active">
            <SvgWorks height={150} width={550}/>
          </div>
          <h2 className="title-big">Мои работы</h2>
        </div>
      </div>
      <PortfolioSlider/>
    </section>
  )
}