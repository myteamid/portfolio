import React, { Component } from 'react'
/*img*/
import bg from '../../assets/images/bg-contact.png'

import './style.scss'

export class ContactForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
      name: '',
      email: '',
      text_message: '',
      formErrors: {email: '', name: ''},
      emailValid: false,
      nameValid: false,
      formValid: false,
    }

    this.defaultValue = this.state
    this.validClass = 'is-valid'
    this.inValidClass = 'is-invalid'
  };

  validateField = (fieldName, value) => {
    let {formErrors, emailValid, nameValid} = this.state

    switch (fieldName) {
      case 'email' :
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
        formErrors.email = emailValid ? this.validClass : this.inValidClass
        break
      case 'name' :
        nameValid = value.length >= 4
        formErrors.name = nameValid ? this.validClass : this.inValidClass
        break
      default:
        break
    }

    this.setState({
      formErrors,
      emailValid,
      nameValid,
    }, this.validateForm)
  }

  validateForm = () => {
    this.setState({
      formValid: this.state.emailValid &&
        this.state.nameValid,
    })
  }

  handleChange = (e) => {
    const {value, name} = e.target
    this.setState({[name]: value},
      () => { this.validateField(name, value) })
  }

  resetForm = () => { this.setState(this.defaultValue) }

  render () {
    const {name, email, text_message, formErrors} = this.state

    const styleSection = {
      backgroundImage: `url(${bg})`,
    }

    return (
      <section className="contact-form" style={styleSection}>
        <div className="contact-form_inner">
          <div className="contact-form_title">
            <h3 className="title-medium line">Связаться со мной</h3>
          </div>
          <div className="contact-form_wrap-form">
            <div className="contact-form_blur" style={styleSection}></div>
            <form id='myform' className="form">
              <div className="form_inner">
                <div className="form_block">
                  <label htmlFor="name">
                    <input type="text" name='name' placeholder='Имя'
                           className={`input ${ formErrors.name } `}
                           value={name} onChange={this.handleChange}/>
                  </label>
                </div>
                <div className="form_block">
                  <label htmlFor="email">
                    <input type="mail" name='email' placeholder='Email'
                           className={`input ${ formErrors.email } `}
                           value={email} onChange={this.handleChange}/>
                  </label>
                </div>
                <div className="form_block">
                  <label htmlFor="text_message">
											<textarea name="text_message" cols="30" rows="10"
                                placeholder='Ваше сообщение' className='input'
                                value={text_message}
                                onChange={this.handleChange}>
											</textarea>
                  </label>
                </div>
              </div>
              <div className="form_btn">
                <button type='submit' disabled={!this.state.formValid}
                        className='btn-main'>
                  Отправить
                </button>
                <button type='reset' className='btn-main'
                        onClick={this.resetForm}>Очистить
                </button>
              </div>
            </form>
          </div>
        </div>
      </section>
    )
  }
}
