import React from 'react'
import PropTypes from 'prop-types'
import 'react-circular-progressbar/dist/styles.css'
/*custom*/
import { SkillItem } from './skillItem'
/*style*/
import './style.scss'

export let Skills = ({data}) => {

  let renderSkillItem = (item) => (
    item.map(skill =>
      <li className="skills_item" key={skill.id}>
        <SkillItem value={skill.level} name={skill.name}/>
      </li>,
    )
  )

  return (
    <ul className="skills">
      {
        data.map(skill => (
          <li className="skills_group" key={skill.id}>
            <h4 className="skills_title">{skill.name}</h4>
            <ul className="skills_list">
              {renderSkillItem(skill.skillItem)}
            </ul>
          </li>
        ))
      }
    </ul>
  )
}

Skills.propTypes = {
  data: PropTypes.array.isRequired,
}