import React, { Component } from 'react'
import CircularProgressbar from 'react-circular-progressbar'
import Waypoint from 'react-waypoint'
import PropTypes from 'prop-types'

export class SkillItem extends Component {
  static get propTypes () {
    return {
      value: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }
  }

  state = {
    levelStart: 0,
    level: this.props.value,
  }

  _handleWaypointEnter = (e) => {
    this.setState({
      levelStart: this.state.level,
    })
  }

  _handleWaypointLeave = (e) => {
    this.setState({
      levelStart: this.state.levelStart,
    })
  }

  componentDidMount () {
    this.setState({
      levelStart: this.state.levelStart,
    })

    this._handleWaypointEnter()
    this._handleWaypointLeave()
  }

  render () {
    const PathStyles = {
      path: {
        opacity: `${this.props.value / 100}`,
      },
    }
    return (
      <Waypoint scrollableAncestor={window} onEnter={this._handleWaypointEnter}
                onLeave={this._handleWaypointLeave}>
        <div className="skills_circle">
          <h5 className="skills_name">{this.props.name}</h5>
          <CircularProgressbar percentage={this.state.levelStart}
                               textForPercentage={null} strokeWidth={15}
                               initialAnimation={true} styles={PathStyles}/>
        </div>
      </Waypoint>
    )
  }

}