import React from 'react'
import PropTypes from 'prop-types'


export function Arrow (props) {
  return (
    <div className={props.className} onClick={props.onClick}>
      <button className="portfolio-slider_btn-inner">
        {props.children}
      </button>
    </div>
  )
}

Arrow.propTypes = {
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}
