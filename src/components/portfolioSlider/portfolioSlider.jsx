import React, { Component } from 'react'
import Slider from 'react-slick'

import { SliderImages } from '../../api/Portfolio'
/*custom*/
import { PortfolioSlide } from '../portfolioSlide'
import { Arrow } from './sliderArrow'
import { PortfolioSlideContent } from '../portfolioSlideContent'
/*img*/
import ArrowPrev from '../../assets/images/icon/portf_arrow_down.svg'
import ArrowNext from '../../assets/images/icon/portf_arrow_up.svg'
/*styles*/
import './style.scss'

export class PortfolioSlider extends Component {

  state = {
    activeSlide: 0,
    activeContent: 0,
    activeSlideLeft: SliderImages.length - 1,
    activeSlideRight: 1,
    direction: 'next',
  }

  handleBeforeChangeActive = (_, current) => {
    this.state.direction === 'next' ? this.setNext(current) : this.setPrev(current)

    this.slickGo()
  }

  setPrev = (current) => {
    const next = Math.abs(
      current + 1 > SliderImages.length - 1 ? 0 : current + 1)

    this.setState({
      activeSlide: current,
      activeSlideLeft: current - 1,
      activeSlideRight: next,
      activeContent: current,
    })
  }

  setNext = (current) => {
    const prev = Math.abs(
      current - 1 < 0 ? SliderImages.length - 1 : current - 1)

    this.setState({
      activeSlide: current,
      activeSlideLeft: prev,
      activeSlideRight: current + 1,
      activeContent: current,
    })
  }

  slickGo = () => {
    this.sliderContent.slickGoTo(this.state.activeContent)
    this.sliderRight.slickGoTo(this.state.activeSlideRight)
    this.sliderLeft.slickGoTo(this.state.activeSlideLeft)
  }

  handlePrev = () => {
    this.sliderActive.slickPrev()
    this.setState({
      direction: 'prev',
    })
  }

  handleNext = () => {
    this.sliderActive.slickNext()
    this.setState({
      direction: 'next',
    })
  }

  navSliderRender = (className) => {
    return SliderImages.map((slide, i) =>
      <PortfolioSlide key={slide.id} image={slide.images} id={i}
                      className={className}/>)
  }

  contentSliderRender = () => SliderImages.map(
    slide => <PortfolioSlideContent key={slide.id} slide={slide}/>)

  render () {
    const settingsSliderLeft = {
      dots: false,
      arrow: false,
      speed: 500,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: SliderImages.length - 1,
    }
    const settingsSliderRight = {
      dots: false,
      arrow: false,
      speed: 500,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 1,
      swipe: false,
      swipeToSlide: false,
      touchMove: false,
    }
    const settingsSliderActive = {
      dots: false,
      arrow: false,
      speed: 500,
      fade: true,
      initialSlide: 0,
      slidesToShow: 1,
      slidesToScroll: 1,
      swipe: false,
      swipeToSlide: false,
      touchMove: false,
      beforeChange: (_, next) => this.handleBeforeChangeActive(_, next),
    }
    const settingsSliderContent = {
      dots: false,
      arrow: false,
      speed: 300,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      swipe: false,
      swipeToSlide: false,
      touchMove: false,
    }
    return (
      <div className="portfolio-slider">
        <div className="portfolio-slider_left">
          <Slider ref={c => (this.sliderContent = c)}
                  {...settingsSliderContent}>
            {this.contentSliderRender()}
          </Slider>
        </div>
        <div className="portfolio-slider_right">

          <div className="portfolio-slider_nav-slider-active">
            <div className="portfolio-slider_nav-slider-active-wrap">
              <Slider ref={c => (this.sliderActive = c)}
                      {...settingsSliderActive}>
                {this.navSliderRender('portfolio-slider_nav-active')}
              </Slider>
            </div>
          </div>

          <div className="portfolio-slider_nav-inner">

            <div className="portfolio-slider_nav-left">
              <Slider ref={c => (this.sliderLeft = c)}
                      {...settingsSliderLeft}>
                {this.navSliderRender('portfolio-slider_nav-item')}
              </Slider>
              <Arrow className='portfolio-slider_btn portfolio-slider_btn--prev'
                     onClick={this.handlePrev}>
                <ArrowPrev width={50} height={50}/>
              </Arrow>
            </div>

            <div className="portfolio-slider_nav-right">
              <Slider ref={c => (this.sliderRight = c)}
                      {...settingsSliderRight}>
                {this.navSliderRender('portfolio-slider_nav-item')}
              </Slider>
              <Arrow className='portfolio-slider_btn portfolio-slider_btn--next'
                     onClick={this.handleNext}>
                <ArrowNext width={50} height={50}/>
              </Arrow>
            </div>

          </div>
        </div>
      </div>
    )
  }
}
