import React, { Component } from 'react'

/*custom*/
import MyMapComponent       from './map'

/*styles for map*/
import stylesMap            from "../../api/GMapStyles";

/*img*/
import Skype                from '../../assets/images/icon/skype.svg'
import Mail                 from '../../assets/images/icon/envelope2.svg'
import Phone                from '../../assets/images/icon/phone.svg'
import Marker               from '../../assets/images/icon/map_marker.svg'

/*style*/
import './style.scss'

export class Gmap extends Component {

    render() {
		const MY_API_KEY = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA79zfHqkR0AT0tKjsuHzRQ2_CKIauOvZA&v=3.exp&libraries=geometry,drawing,places';

		return (

			<section className="g-map">
				<div className="contact">
					<h3 className="contact_title title-medium">Контакты</h3>
					<ul className="contact_list">
						<li className="contact_item">
							<div className="contact_icon">
								<Skype width={30} height={30}></Skype>
							</div>
							<a href="">
								skype_name
							</a>
						</li>
						<li className="contact_item">
							<div className="contact_icon">
								<Mail width={30} height={30}></Mail>
							</div>
							<a href="">
								skype_name
							</a>
						</li>
						<li className="contact_item">
							<div className="contact_icon">
								<Phone width={30} height={30}></Phone>
							</div>
							<a href="">
								skype_name
							</a>
						</li>
						<li className="contact_item">
							<div className="contact_icon">
								<Marker width={30} height={30}></Marker>
							</div>
							<a href="">
								г.Новосибирск, ул. Инская, д. 5
							</a>
						</li>
					</ul>
				</div>

				<MyMapComponent
					isMarkerShown
					googleMapURL={MY_API_KEY}
					loadingElement={<div style={{height: `100%`}}/>}
					containerElement={<div className={'g-map_inner'}/>}
					mapElement={<div style={{height: `100%`}}/>}
					mapStyles={stylesMap}
				/>

			</section>
        )
    }
}
