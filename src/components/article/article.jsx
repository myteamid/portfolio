import React from 'react'

/*styles*/
import './style.scss'

export const Article = () => {

    return (
        <article className="article-blog">
            <header className="article-blog_header">
                <h2 className="article-blog_title">Lorem ipsum dolor sit amet.</h2>
                <time>22 ноября 2016</time>
            </header>
            <div className="article-blog_content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet cumque
                    debitis enim esse eum explicabo fugiat libero modi nesciunt nostrum possimus qui
                    ratione reiciendis rem, suscipit tempora voluptates voluptatum?</p>
                <pre>
                    <code className="content-code">
                      div.blog_right h2 {

                    }
                    </code>
                </pre>

            </div>
        </article>
    );
};

