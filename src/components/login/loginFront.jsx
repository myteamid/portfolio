import React from 'react'
import { Link } from 'react-router-dom'
/*custom*/
import { User } from '../user'
import { Socials } from '../socials'
import routing from '../../api/Routing'

export const LoginFront = () => {

  const renderNav = () => routing.map(item => {
    if (item.path === '/login') return null
    return (
      <Link className="flip_link" to={item.path} key={item.id}>
        <span>{item.title}</span>
      </Link>
    )
  })

  return (
    <div className="flip_front">
      <div className="flip_user">
        <User className='user--login'>Личный сайт веб разработчика</User>
      </div>
      <div className="flip_socials">
        <Socials/>
      </div>
      <div className="flip_btn-front">
        {renderNav()}
      </div>
    </div>
  )
}