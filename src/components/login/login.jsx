import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CSSTransitionGroup from 'react-addons-css-transition-group'

/*custom*/
import { LoginFront } from './loginFront'
import { LoginBack }from './loginBack'

/*style*/
import './style.scss'

export class Login extends Component {

  static get propTypes () {
    return {
      loginBg: PropTypes.array.isRequired,
    }
  }

  state = {
    backActive: false,
  }

  handleClick = (e) => {
    this.setState({backActive: !this.state.backActive})
  }

  activeClass = () => this.state.backActive ? 'flip-back-active' : 'flip-front-active'

  renderAuthBtn = () => (
    <CSSTransitionGroup transitionName="login-btn"
                        transitionEnterTimeout={300}
                        transitionLeaveTimeout={150}>
      {!this.state.backActive ?
        <div className="container-fluid">
          <div className="login_btn">
            <button className="btn--transparent"
                    onClick={this.handleClick}>
              Авторизоваться
            </button>
          </div>
        </div> : ''}
    </CSSTransitionGroup>
  )

  render () {
    const {backActive} = this.state
    const {loginBg} = this.props
    const [bgLogin] = loginBg

    const styleLogin = {backgroundImage: `url(${bgLogin.bgHero})`}

    return (
      <div className={`login ${this.activeClass()}`} style={styleLogin}>
        {this.renderAuthBtn()}

        <div className="login_inner">
          <div className="flip">
            <CSSTransitionGroup
              transitionName="flip-animation"
              transitionEnterTimeout={700}
              transitionLeaveTimeout={500}>
              {backActive ?
                <LoginBack handleClick={this.handleClick} key={0}/>
                : <LoginFront key={1}/>}

            </CSSTransitionGroup>
          </div>
        </div>
      </div>

    )
  }

}