import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Input } from '../input'
import { FormButton } from '../formButton'
/*img*/
import LoginIcon from '../../assets/images/icon/login.svg'
import PassIcon from '../../assets/images/icon/password.svg'

export class LoginBack extends Component {
  static propTypes = {
    handleClick: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this.state = {
      loginName: '',
      loginPass: '',
      formErrors: {loginPass: '', loginName: ''},
      loginNameValid: false,
      loginPassValid: false,
      checkHuman: false,
      checkHumanRadio: false,
      formValid: false,
    }

    this.validClass = 'is-valid'
    this.inValidClass = 'is-invalid'
  };

  validateField = (fieldName, target) => {
    let {
      formErrors,
      loginPassValid,
      loginNameValid,
      checkHuman,
      checkHumanRadio,
    } = this.state

    switch (fieldName) {
      case 'loginPass' :
        loginNameValid = target.value.length >= 4
        formErrors.loginPass = loginNameValid
          ? this.validClass
          : this.inValidClass
        break
      case 'loginName' :
        loginPassValid = target.value.length >= 6
        formErrors.loginName = loginPassValid
          ? this.validClass
          : this.inValidClass
        break
      case 'checkHuman' :
        checkHuman = target.checked
        break
      case 'checkHumanRadio' :
        if (target.value === 'noSubmit') {
          checkHumanRadio = false
        } else {
          checkHumanRadio = true
        }
        break
      default:
        break
    }

    this.setState({
      formErrors,
      loginPassValid,
      loginNameValid,
      checkHuman,
      checkHumanRadio,
    }, this.validateForm)

  }

  validateForm = () => {
    const {loginNameValid, loginPassValid, checkHuman, checkHumanRadio} = this.state
    this.setState({
      formValid: loginNameValid && loginPassValid && checkHuman &&
        checkHumanRadio,
    })
  }

  handleChange = (e) => {
    const target = e.target
    const {name, value} = target
    this.setState({[name]: value},
      () => { this.validateField(name, target) })
  }

  render () {
    const {handleClick} = this.props
    const {formErrors} = this.state
    return (
      <div className="flip_back">
        <div className="flip_back-title">
          <h3 className="title line title-medium">Авторизоваться</h3>
        </div>
        <form className="form flip_form" id="form-login">
          <div className="flip_inner">
            <div className="form_block">
              <label htmlFor="loginName"
                     className={`input-icon ${formErrors.loginName}`}>
                <span><LoginIcon wight={20} height={20}/></span>
                <input type="text" name='loginName' placeholder="Логин"
                       className="input" onChange={this.handleChange}/>
              </label>
            </div>
            <div className="form_block">
              <label htmlFor="loginPass"
                     className={`input-icon ${formErrors.loginPass}`}>
                <span><PassIcon wight={20} height={20}/></span>
                <input type="password" name='loginPass' placeholder="Пароль"
                       className="input" onChange={this.handleChange}/>
              </label>
            </div>
            <div className="form_check">
              <Input onChange={this.handleChange} classNameLabel="checkbox"
                     className="checkbox_icon" type="checkbox"
                     name="checkHuman">
                Я человек
              </Input>
            </div>
            <div className="form_check">
              <span className="form_check-title">Вы точно не робот?</span>
              <Input onChange={this.handleChange} classNameLabel="radio"
                     className="checkbox_icon" type="radio"
                     name="checkHumanRadio">
                <span>Да</span>
              </Input>
              <Input onChange={this.handleChange} classNameLabel="radio"
                     className="checkbox_icon" value="noSubmit" type="radio"
                     name="checkHumanRadio">
                <span>Не уверен</span>
              </Input>
            </div>
          </div>
        </form>
        <div className="flip_btn-back">
          <FormButton className="flip_btn-item flip_link" onClick={handleClick}>
            На главную
          </FormButton>
          <FormButton type='submit' form="form-login" disabled={!this.state.formValid}
                      className="flip_btn-item flip_link">
            Войти
          </FormButton>
        </div>
      </div>
    )
  }
}