import { connect } from 'react-redux'
import {Login as LoginClass} from './login'

function mapStateToProps (state) {
  return {
    loginBg: state.activeTheme.images,
  }
}

export let Login = connect(mapStateToProps)(LoginClass)