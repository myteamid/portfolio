import { connect } from 'react-redux'
import { Hero as HeroClass } from './hero'

function mapStateToProps(state) {
  return {
    heroBg: state.activeTheme.images
  }
}

export const Hero = connect(mapStateToProps)(HeroClass)