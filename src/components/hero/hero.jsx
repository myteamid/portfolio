import React, { Component } from 'react'
import PropTypes from 'prop-types'
/*custom components*/
import { User } from '../user'
/*img*/
import ArrowDown from '../../assets/images/icon/arrow_down.svg'
/*style*/
import './style.scss'

export class Hero extends Component {
  static get propTypes () {
    return {
      heroBg: PropTypes.array.isRequired,
    }
  }

  render () {
    const {heroBg, children} = this.props
    const [bgHero] = heroBg
    const heroStyles = {
      backgroundImage: `url(${bgHero.bgHero})`,
    }
    return (
      <section className="hero" style={heroStyles}>
        <div className="hero_content">
          <div className="hero_content-bg"></div>
          <User>
            {children}
          </User>
        </div>
        <div className="hero_btn">
          <a href="#0" className="btn-scroll">
            <ArrowDown wight={20} heght={20}/>
          </a>
        </div>
      </section>
    )
  }
}
