import React from 'react'
/*img*/
import Link from '../../assets/images/icon/link.svg'

export const PortfolioSlideContent = (props) => {
  function renderSkills (arr) {
    return arr.map(skill => <li key={skill.id}>{skill.name}</li>)
  }

  return (
    <div className="portfolio-slider_inner">
      <div className="portfolio-slider_content">
        <h4 className="title-medium line">{props.slide.title}</h4>
        <ul className="portfolio-slider_skills">
          {renderSkills(props.slide.technology)}
        </ul>
        <div className="portfolio-slider_link">
          <a href={props.slide.link} className="btn-icon">
            <span className='btn-icon_icon'>
              <Link width={20} height={20}/></span>
            <span className='btn-icon_text'>Посмотреть сайт</span>
          </a>
        </div>
      </div>
    </div>
  )
}

