import React, { Component } from 'react'
import PropTypes from 'prop-types'

/*custom*/
import { Button } from './button'
import { themeData } from '../../api/themesData'
import { findToIndex } from '../../helpers/toggleThemes'

/*style*/
import './style.scss'

export class ColorSwitcher extends Component {
  static get propTypes () {
    return {
      activeTheme: PropTypes.object.isRequired
    }
  }

  toggleTheme = (e) => {
    const targetColor = Number(e.target.dataset.id);
    if (!this.props.activeTheme.id === targetColor) return
    this.props.toggleTheme(findToIndex(targetColor, themeData))
  }

  renderButton = () => (
    themeData.map((item) => (
      <li className="color-switcher_item" key={item.id}>
        <Button color={item.color}
                id={item.id}
                activeColor={item.id === this.props.activeTheme.id}
                toggleTheme={this.toggleTheme}>
        </Button>
        <span className="color-switcher_item-text">{item.title}</span>
      </li>
    ))
  )

  render () {
    return (
      <ul className="color-switcher">
        {this.renderButton()}
      </ul>
    )
  }
}