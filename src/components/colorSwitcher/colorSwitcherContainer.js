import { ColorSwitcher as ColorSwitcherClass } from './colorSwitcher'
import { connect } from 'react-redux'
import { toggleTheme } from '../../AC'

function mapStateToProps (state) {
  return {
    activeTheme: state.activeTheme,
  }
}

const mapDispatchToProps = {toggleTheme}

export const ColorSwitcher = connect(mapStateToProps, mapDispatchToProps)(ColorSwitcherClass)
