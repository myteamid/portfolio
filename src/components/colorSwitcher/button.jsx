import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

export const Button = ({ color, id, toggleTheme, children, activeColor }) => {
    const activeCl = cn({
      'is-active': activeColor,
    });

  return (
        <button className={`color-switcher_btn color-switcher_btn--${color} ${activeCl}`}
                onClick={toggleTheme}
                data-id={id}
        >
            {children}
        </button>
    )
};

Button.propTypes = {
    color: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    toggleTheme: PropTypes.func.isRequired,
    children: PropTypes.element,
    activeColor: PropTypes.bool.isRequired
};