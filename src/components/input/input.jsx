import React from 'react'
import PropTypes from 'prop-types'

import './style.scss'

export const Input = (props) => {
  const {
    className,
    children,
    classNameLabel,
    htmlID,
    ...rest
  } = props

  return (
    <label className={classNameLabel} htmlFor={htmlID}>
      <input id={htmlID}
             {...rest}/>
      <span className={className}> {children} </span>
    </label>
  )
}

Input.propTypes = {
  htmlID: PropTypes.string,
  className: PropTypes.string,
  classNameLabel: PropTypes.string,
  children: PropTypes.any,
}