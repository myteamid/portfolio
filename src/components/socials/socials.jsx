import React from 'react'
import PropTypes from 'prop-types'
/*img*/
import GithubICon from '../../assets/images/icon/github.svg'
/*style*/
import './style.scss'

export const Socials = ({className = ''}) => {

  return (
    <ul className={`socials ${className}`}>
      <li className='socials_item'>
        <a href="#0">
          <GithubICon width={25} height={25}/>
        </a>
      </li>
      <li className='socials_item'>
        <a href="#0">
          <GithubICon width={25} height={25}/>
        </a>
      </li>
    </ul>
  )
}

Socials.propTypes = {
  className: PropTypes.string,
}
