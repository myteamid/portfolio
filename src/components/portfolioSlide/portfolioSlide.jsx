import React from 'react'
import PropTypes from 'prop-types'

export function PortfolioSlide ({image, id, className}) {
	const styles = {
		backgroundImage: `url(${image})`
	};
    return (
			<div className={className}
				 style={styles}>
				<span>{id}</span>
			</div>
	);
}

PortfolioSlide.propTypes = {
	image: PropTypes.string.isRequired,
	id: PropTypes.number,
	className: PropTypes.string.isRequired
}