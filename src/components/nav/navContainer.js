import { connect } from 'react-redux'
import ToggleComponent from '../../decorators/toggleComponent'
import { Nav as NavClass } from './nav'

function mapStateToProps (state) {
  return {
    open: state.openMenu,
  }
}

const decorator = connect(mapStateToProps)

export const Nav = decorator(ToggleComponent(NavClass))