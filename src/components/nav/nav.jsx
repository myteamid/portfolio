import React, { Component } from 'react'
import classNames from 'classnames'
/*custom*/
import { ColorSwitcher } from '../colorSwitcher'
import { NavLink } from 'react-router-dom'
import routing from '../../api/Routing'

/*styles*/
import './styles.scss'

export class Nav extends Component {

  renderNavList = () => (
    routing.map(item => (
      <li className="nav_item" key={item.id}>
        <NavLink exact to={item.path} className="nav_link">
							<span data-text={item.title}>
								<span>{item.title}</span>
							</span>
        </NavLink>
      </li>
    ))
  )

  render () {
    const {open} = this.props
    const activeClass = classNames({
      'nav-wrapper': true,
      'is-active': open
    })

    return (
      <div className={activeClass}>
        {open ?
          <React.Fragment>
            <nav className="nav js-nav">
              <ul className="nav_list">
                {this.renderNavList()}
              </ul>
            </nav>
            <ColorSwitcher/>
          </React.Fragment>
          :
          null
        }
      </div>
    )
  }
}
