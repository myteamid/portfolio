import { toggleMenu } from '../../AC'
import { connect } from 'react-redux'

import { Sandwich as SandwichContainer } from './sandwich'
import ToggleComponent from '../../decorators/toggleComponent'

function mapStateToProps (state) {
  return {
    open: state.openMenu,
  }
}

const mapDispatchToProps = {toggleMenu}

const decorator = connect(mapStateToProps, mapDispatchToProps)

export let Sandwich = decorator(ToggleComponent(SandwichContainer))