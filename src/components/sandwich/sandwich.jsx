import React, { Component } from 'react'
import classNames from 'classnames'
/*styles*/
import './style.scss'

export class Sandwich extends Component {

  render () {
    const {open, toggleOpenComponent} = this.props
    const activeClass = classNames({
      'sandwich': true,
      'is-active': open
    })
    return (
      <div className={activeClass}>
        <button className='sandwich_inner' onClick={toggleOpenComponent}>
          <span className="sandwich_top"> </span>
          <span className="sandwich_middle"> </span>
          <span className="sandwich_bottom"> </span>
        </button>
      </div>
    )
  }
}