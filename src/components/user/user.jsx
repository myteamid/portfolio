import React from 'react'
import PropTypes from 'prop-types'
/*img*/
import UserImg from '../../assets/images/ava.jpg'
/*styles*/
import './style.scss'

export const User = ({className = '', children}) => {

  return (
    <div className={`user ${className}`}>
      <div className="user_img">
        <img src={UserImg} alt="User Avatar"/>
      </div>
      <h3 className="user_name">Роман Таран</h3>
      <p className="user_descr">{children}</p>
    </div>
  )
}

User.propTypes = {
  className: PropTypes.string,
}