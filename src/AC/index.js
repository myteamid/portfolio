import {
    CLOSE_MENU,
    OPEN_MENU,
    IS_LOAD_THEME_ERROR,
    IS_LOAD_THEME_LOADING,
    IS_LOAD_THEME_SUCCESS,
    IS_LOAD_THEME_ITEM_SUCCESS,
    TOGGLE_THEME
} from "../constants";


export function toggleMenu() {
    return {
        type: OPEN_MENU
    }
}

export function toggleTheme(target) {
    return {
        type: TOGGLE_THEME,
        targetTheme: target
    }
}

export function closeMenu() {
    return {
        type: CLOSE_MENU
    }
}


export function isLoadThemeError(bool, error) {
    console.log(error);
    return {
        type: IS_LOAD_THEME_ERROR,
        hasError: bool
    }
}

export function isLoadTheme(bool) {
    return {
        type: IS_LOAD_THEME_LOADING,
        isLoading: bool
    }
}

export function loadThemeData(themeColor) {
    return {
        type: IS_LOAD_THEME_SUCCESS,
        dataThemeColor: themeColor
    }
}

export function loadItemData(themeItemData) {
    return {
        type: IS_LOAD_THEME_ITEM_SUCCESS,
        themeItemData
    }
}

export function themeFetchData(url) {
    return (dispatch) => {
        dispatch(isLoadTheme(true));

        fetch(url)
            .then(response => {
                if ( !response.ok ) {
                    throw Error(response.statusText);
                }
                dispatch(isLoadTheme(false));
                return response;
            })
            .then(response => response.json())
            .then(theme => dispatch(loadThemeData(theme)))
            .catch(error => dispatch(isLoadThemeError(true, error)))
    }
}

export function themeItemFetch(url) {
    return (dispatch) => {
        dispatch(isLoadTheme(true));

        fetch(url)
            .then(response => {
                if (!response.ok) throw Error(response.statusText);
                dispatch(isLoadTheme(false));
                return response;
            })
            .then(response => response.json())
            .then(data => dispatch(loadItemData(data)))
            .catch(error => dispatch(isLoadThemeError(true, error)))
    }
}

