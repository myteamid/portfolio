import React from 'react'

export default Component => class ToggleComponent extends Component {

	render() {
		return (
			<Component {...this.props} toggleOpenComponent={this.toggleOpenComponent} openComponent={this.props.open}/>
		)
	}

	toggleOpenComponent = () => {
		this.props.toggleMenu()
	}
}